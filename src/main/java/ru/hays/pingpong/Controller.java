package ru.hays.pingpong;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.sql.*;

@RestController
@PropertySource("classpath:db.properties")
public class Controller {

  private Connection conn;
  private ObjectMapper mapper = new ObjectMapper();

  @Value("${path.project}")
  private String path;

  @RequestMapping(value = "/handler", method = RequestMethod.POST)
  public String index(@RequestBody String json) throws ClassNotFoundException, SQLException, IOException {
    if (conn == null) {
      Class.forName("org.hsqldb.jdbcDriver");
      conn = DriverManager.getConnection("jdbc:hsqldb:file:" + path + "/src/main/resources/db\\./",
          "sa",
          "");
      System.out.println("Created new connection to db");
    }
    String result = "";

    Entry entry = mapper.readValue(json, Entry.class);
    switch (entry.getCommand().toLowerCase()) {
      case "ping":
        result = processPing(Integer.valueOf(entry.getParams().get("userId")));
        break;
      default:
        System.out.println("Not implemented command: " + entry.getCommand());
    }


    return result;
  }

  private void setNum(int userId, int num) throws SQLException {
    String sql = num == 1
        ? String.format("INSERT INTO REQUESTS VALUES (%d, %d)", userId, num)
        : String.format("UPDATE REQUESTS SET NUM=%d WHERE ID = %d", num, userId);
    conn.createStatement().executeUpdate(sql);
  }

  private String processPing(int userId) throws SQLException {
    Statement st = conn.createStatement();
    int newN = 1;
    String sql = "SELECT num FROM requests where id=" + userId;
    ResultSet resultSet = st.executeQuery(sql);
    if (resultSet.next()) {
      newN = resultSet.getInt(1) + 1;
    }
    setNum(userId, newN);
    return "PONG " + newN;
  }

}