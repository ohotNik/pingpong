package ru.hays.pingpong;

import java.util.Map;

public class Entry {

  private String command;
  private Map<String, String> params;

  public String getCommand() {
    return command;
  }

  public Map<String, String> getParams() {
    return params;
  }
}
